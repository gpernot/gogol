package gogol

import (
	"errors"
	"fmt"
	"strings"
	"sync"
)

type Quadrant int

const (
	ROOT Quadrant = iota
	TOP_LEFT
	TOP_RIGHT
	BOTTOM_LEFT
	BOTTOM_RIGHT
	INVALID
)

func (q Quadrant) next() (Quadrant, error) {
	switch q {
	case ROOT:
		return INVALID, errors.New("Invalid next() on ROOT board")
	case TOP_LEFT:
		return TOP_RIGHT, nil
	case TOP_RIGHT:
		return BOTTOM_LEFT, nil
	case BOTTOM_LEFT:
		return BOTTOM_RIGHT, nil
	case BOTTOM_RIGHT:
		return INVALID, nil
	default:
		return INVALID, errors.New("Invalid Quadrant state")
	}

}

type Board struct {
	quadrant Quadrant
	size     int
	active   *int
	content  [2][][]bool
	children [2][2]*Board
}

func NewBoard(size int) *Board {
	active := 0
	board := new(Board)
	board.quadrant = ROOT
	board.size = size
	board.active = &active
	board.content[0] = make([][]bool, size)
	board.content[1] = make([][]bool, size)
	for i := 0; i < size; i++ {
		board.content[0][i] = make([]bool, size)
		board.content[1][i] = make([]bool, size)
	}
	board.CreateSubBoards()
	return board
}

func (board *Board) CreateSubBoards() {
	var err error
	if board.size < 8 {
		return
	}
	child_size := board.size / 2
	// upper-left sub-board
	quadrant := TOP_LEFT
	for i := 0; i < 2; i++ {
		for j := 0; j < 2; j++ {
			board.children[i][j] = new(Board)
			board.children[i][j].quadrant = quadrant
			board.children[i][j].size = child_size
			board.children[i][j].active = board.active
			board.children[i][j].content[0] = make([][]bool, child_size)
			board.children[i][j].content[1] = make([][]bool, child_size)
			for k := 0; k < child_size; k++ {
				board.children[i][j].content[0][k] = board.content[0][i*child_size+k][j*child_size : (j+1)*child_size]
				board.children[i][j].content[1][k] = board.content[1][i*child_size+k][j*child_size : (j+1)*child_size]
			}
			board.children[i][j].CreateSubBoards()
			quadrant, err = quadrant.next()
			if err != nil {
				panic(err)
			}
		}
	}
}

func (board *Board) swap() {
	*board.active = 1 - *board.active
}

func (board *Board) neighbours(i, j int) int {
	count := 0
	cells := board.content[*board.active]
	for x := -1; x <= 1; x++ {
		for y := -1; y <= 1; y++ {
			if (x != 0 || y != 0) &&
				(x+j >= 0 && x+j < int(board.size) && y+i >= 0 && y+i < int(board.size)) &&
				cells[y+i][x+j] {
				count++
			}
		}
	}
	return count
}

func (board *Board) apply_rule(i, j int) {
	old_cells := board.content[*board.active]
	new_cells := board.content[1-*board.active]
	count := board.neighbours(i, j)
	switch {
	case old_cells[i][j] && (count == 2 || count == 3):
		new_cells[i][j] = true
	case !old_cells[i][j] && count == 3:
		new_cells[i][j] = true
	default:
		new_cells[i][j] = false
	}
}

func (board *Board) inner_step() {
	for i := 1; i < int(board.size-1); i++ {
		for j := 1; j < int(board.size-1); j++ {
			board.apply_rule(i, j)
		}
	}
}
func (board *Board) boundaries_step() {

	if board.quadrant == ROOT {
		// left
		for i := 0; i < board.size; i++ {
			board.apply_rule(i, 0)
		}

		// right
		for i := 0; i < board.size; i++ {
			board.apply_rule(i, board.size-1)
		}
		// top
		for j := 1; j < board.size-1; j++ {
			board.apply_rule(0, j)
		}

		// bottom
		for j := 1; j < board.size-1; j++ {
			board.apply_rule(board.size-1, j)
		}
	}
	// vertical bands
	for i := 1; i < int(board.size-1); i++ {
		// middle-left
		board.apply_rule(i, board.size/2-1)

		// middle-right
		board.apply_rule(i, board.size/2)
	}

	// horizontal bands
	for j := 1; j < int(board.size-1); j++ {
		// middle-top
		board.apply_rule(board.size/2-1, j)

		// middle-bottom
		board.apply_rule(board.size/2, j)

	}
}

func (board *Board) Step() {

	if board.children[0][0] != nil {
		var wg sync.WaitGroup
		wg.Add(5)
		for i := 0; i < 2; i++ {
			for j := 0; j < 2; j++ {
				go func(i_, j_ int) {
					defer wg.Done()
					board.children[i_][j_].Step()
				}(i, j)
			}
		}
		go func() {
			defer wg.Done()
			board.boundaries_step()
		}()
		wg.Wait()
	} else {
		board.inner_step()
	}

	if board.quadrant == ROOT {
		board.swap()
	}
}

func (board *Board) Set(x, y int, value bool) {
	if x > 0 && x < int(board.size) && y > 0 && y < int(board.size) {
		board.content[*board.active][y][x] = value
	}
}

func (board *Board) Get(x, y int) bool {
	return board.content[*board.active][y][x]
}

func (board *Board) IsDirty(x, y int) bool {
	return board.content[0][y][x] != board.content[1][y][x]
}

func (board *Board) Size() int {
	return board.size
}

func (board *Board) Print() {
	for y := 0; y < int(board.size); y++ {
		for x := 0; x < int(board.size); x++ {
			value := board.Get(x, y)
			if value {
				fmt.Print("██")
			} else {
				fmt.Print("  ")
			}
		}
		fmt.Println()
	}
}

func (board *Board) ThrowSpaceship(x, y int) {
	ship := [3][3]bool{{false, false, true},
		{true, false, true},
		{false, true, true}}
	for i := 0; i < len(ship); i++ {
		for j := 0; j < len(ship[i]); j++ {
			board.Set(x+j, y+i, ship[i][j])
		}
	}
}

func (board *Board) ThrowPattern(x, y int, name string) error {
	pattern, err := FromRLE(RLEPatterns[name])
	if err != nil {
		return err
	}
	for i := 0; i < pattern.height; i++ {
		for j := 0; j < pattern.width; j++ {
			board.Set(x+j, y+i, pattern.cells[i][j])
		}
	}
	return nil
}

func flush_cells(b *strings.Builder, count int, state bool) int {
	var cell byte
	var chars int

	if state {
		cell = 'o'
	} else {
		cell = 'b'
	}
	switch {
	case count == 0:
		chars = 0
	case count == 1:
		chars, _ = fmt.Fprintf(b, "%c", cell)
	default:
		chars, _ = fmt.Fprintf(b, "%d%c", count, cell)
	}
	return chars

}

func flush_eol(b *strings.Builder, count int) int {
	var chars int
	switch {
	case count == 0:
	case count == 1:
		chars, _ = fmt.Fprint(b, "$")
	case count > 1:
		chars, _ = fmt.Fprintf(b, "%d$", count)
	}
	return int(chars)
}

func check_max_column(b *strings.Builder, column int) int {
	if column >= 70 {
		fmt.Fprintln(b, "")
		return 0
	}
	return column
}

func (board *Board) Save() (string, error) {
	var res strings.Builder
	var column int
	var x, y int
	var state bool
	var eol_count int

	fmt.Fprintf(&res, "x = %d, y = %d, rule = B3/S23\n", board.size, board.size)

	for y = 0; y < board.size; y++ {
		count := 0
		state = false
		for x = 0; x < board.size; x++ {
			c := board.Get(int(x), int(y))

			if state != c {
				column += flush_eol(&res, eol_count)
				column = check_max_column(&res, column)
				eol_count = 0
				column += flush_cells(&res, count, state)
				column = check_max_column(&res, column)
				state = c
				count = 1
			} else {
				count++
			}
		}
		if state {
			// flush last living cells in line
			column += flush_cells(&res, count, state)
			column = check_max_column(&res, column)
		}
		eol_count++
	}
	fmt.Fprint(&res, "!")

	return res.String(), nil
}
