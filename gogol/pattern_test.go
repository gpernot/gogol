package gogol

import (
	"strings"
	"testing"
)

func TestFromRLE(t *testing.T) {
	_, err := FromRLE(RLEPatterns["Sir Robin"])

	if err != nil {
		t.Errorf("Error: %v", err)
	}

}

func TestParseHeader(t *testing.T) {

	pattern := Pattern{}
	reader := strings.NewReader("x = 13, y = 1, rule = B3/S23\n")

	err := parse_header(reader, &pattern)
	if err != nil || pattern.width != 13 || pattern.height != 1 {
		t.Errorf("Error: err = [%v], width = %d, height = %d",
			err, pattern.width, pattern.height)
	}
}
