package gogol

import (
	"testing"
)

func TestSpaceship(t *testing.T) {
	expected := `x = 128, y = 128, rule = B3/S23
80$33bo$31bo3bo$30bo$30bo4bo$30b5o!`

	board := NewBoard(128)
	board.ThrowPattern(80, 80, "Middleweight spaceship")
	for i := 0; i < 100; i++ {
		board.Step()
	}
	res, _ := board.Save()
	if res != expected {
		t.Errorf("Result mismatch:\n%s\n!=\n%s", res, expected)
	}
}

func TestSirRobin(t *testing.T) {
	expected := `x = 256, y = 256, rule = B3/S23
47$67b3o$66bo2b2o$66bo4bo2$66b3o3b2ob3o$64b2obobo4bob2o$66bo2bo4bo2b2o
$63bo2bo8bo$63bo4bo2b2o2bo2bo$64bob3o2b2o3b2o$66b2obob2obo3bo$72b2ob3o
$72bo5bo$75b3o2b3o$78bob2o$73b2obo3b3o$73b2o2bo2b2o$75b4o3bo$76b2obo2b
o$74b2obobobo$77bo$74bo7b2o$75b2o5bo$76bob2o2bob2o$82bo3bo$75b3o5bo2$79b
3o3bo$75bo2bo$74b2obo$75bob2o3b2o$77b2ob4o$85b3o$75b2ob2o3bo2bo$84bob2o
$84b2o2$84b5o$83b5ob2o$83bo6b2o$84b2o$86b2o$81b3o$86b2ob4o$81bo2b3o2bo
bo$86b2o2b2o$83bo2bo$86bo$83bo2bo$83b5o2$82b2obo$82b2ob2o$81bob2obobo$
81bo3bobo$81b8o$81bo7bo$81bo2bo3b3o$82bo7b2o$87b5o$84bo7bo$86bo$86bo2b
o3bo2$84b2obo2bobo$84b2obo4b2o$88bobo$87b2obob2o$87b2obobo$87b2ob2o$87b
2o3bo$87b4obo$87b4obo$91b3o$92b2o$89b4o$88b4o$88bo!`

	board := NewBoard(256)
	board.ThrowPattern(80, 80, "Sir Robin")
	for i := 0; i < 100; i++ {
		board.Step()
	}
	res, _ := board.Save()
	if res != expected {
		t.Errorf("Result mismatch:\n%s\n!=\n%s", res, expected)
	}
}

func Benchmark117P18(b *testing.B) {
	board := NewBoard(4096)
	board.ThrowPattern(0, 0, "117P18")
	board.ThrowPattern(1024, 1024, "117P18")
	board.ThrowPattern(2048, 2048, "117P18")
	board.ThrowPattern(4000, 4000, "117P18")
	for i := 0; i < b.N; i++ {
		board.Step()
	}
}
