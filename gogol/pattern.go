package gogol

import (
	"errors"
	"strings"
	"io"
	"fmt"
)

type Pattern struct {
	width, height int
	rule string
	cells [][]bool
}

func (pattern *Pattern) fillCells(i, j, count int, value bool) {
	for c := 0; c < count; c++ {
		pattern.cells[i][j+c] = value
	}
}

func (pattern *Pattern) Print() {
	fmt.Printf("width = %d, height = %d\n", pattern.width, pattern.height)
	for i:=0; i<pattern.height; i++ {
		for j:=0; j<pattern.width; j++ {
			value := pattern.cells[i][j]
			if value { fmt.Print("██") } else { fmt.Print("  ") }
		}
		fmt.Println()
	}
}

func parse_comment(reader *strings.Reader) error {
	var err error
	var c byte
	for {
		c, err = reader.ReadByte()
		if err != nil {
			return err
		}
		if c == '\n' {
			return nil
		}
	}
}

/* parse "<var> = <number>" string */
func parse_header_int(reader *strings.Reader) (int, error) {
	var in_number bool
	var number = 0
	var err error
	var c byte

	for {
		c, err = reader.ReadByte()
		if err == io.EOF {
			if in_number {
				return number, nil
			} else {
				return -1, errors.New("Unexpected end-of-file")
			}
		}
		switch c {
		case '=':
			in_number = true
		case ' ', '\t':
			if in_number && number != 0 {
				return -1, errors.New("Parsing error in number")
			}
		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
			if in_number {
				number = number * 10 + int(c - '0')
			} else {
				in_number = true
				number = int(c - '0')
			}
		case ',', '\n':
			return number, nil
		default:
			return -1, errors.New(fmt.Sprintf("Parsing error in number [%c] %d", c, number))
		}
	}
}

/* parse "rule = <string>" string */
/*
func parse_header_rule(reader *Reader) ([]byte, error) {
	var in_string bool
	var rule []byte

	c, err = reader.ReadByte()
	if err != nil || c != 'u' {
		return nil, "Error parsing rule"
	}
	c, err = reader.ReadByte()
	if err != nil || c != 'l' {
		return nil, "Error parsing rule"
	}
	c, err = reader.ReadByte()
	if err != nil || c != 'e' {
		return nil, "Error parsing rule"
	}
	for {
		c, err = reader.ReadByte()
	}

}
*/

func parse_header(reader *strings.Reader, pattern *Pattern) error {
	var err error
	var c byte
	var x_defined, y_defined bool

	for {
		c, err = reader.ReadByte()
		switch err {
		case nil:
		case io.EOF:
			return err
		default:
			return errors.New("Unexpected error parsing RLE")
		}
		switch c {
		case 'x':
			pattern.width, err = parse_header_int(reader)
			if err != nil {
				return err
			}
			x_defined = true
		case 'y':
			pattern.height, err = parse_header_int(reader)
			if err != nil {
				return err
			}
			y_defined = true
			/*
		case 'r':
			pattern.rule, err = parse_header_rule(reader)
			if err != nil {
				return err
			}
			*/
		case ' ', '\t':
		case '\n':
			if !x_defined || !y_defined {
				return errors.New(fmt.Sprintf("Invalid header %d %d", pattern.width, pattern.height))
			}
			return nil
		}
	}
}

func parse_rle(reader *strings.Reader, pattern *Pattern) error {
	var c byte
	var err error
	var in_number bool
	var number int
	var i, j int

	pattern.cells = make([][]bool, pattern.height)
	for i = range pattern.cells {
		pattern.cells[i] = make([]bool, pattern.width)
	}

	i = 0
	j = 0
	in_number = false
	for {
		c, err = reader.ReadByte()
		switch err {
		case nil:
		case io.EOF:
			return err
		default:
			return errors.New("Unexpected error parsing RLE")
		}
		switch c {
		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
			if in_number {
				number = number * 10 + int(c - '0')
			} else {
				in_number = true
				number = int(c - '0')
			}
		case 'b':
			if !in_number {
				number = 1
			}
			pattern.fillCells(i, j, number, false)
			j += number
			in_number = false
		case 'o':
			if !in_number {
				number = 1
			}
			pattern.fillCells(i, j, number, true)
			j += number
			in_number = false
		case '$':
			if !in_number {
				number = 1
			}
			i += number
			j = 0
			in_number = false
		case '!':
			return nil
		}
	}
}

func FromRLE(rle string) (*Pattern, error) {
	var c byte
	var err error

	pattern := Pattern{}
	reader := strings.NewReader(rle)

	for {
		c, err = reader.ReadByte()
		if err == io.EOF {
			return &pattern, nil
		} else if err != nil {
			return nil, err
		}
		switch c {
		case '#':
			err = parse_comment(reader)
		case 'x':
			reader.UnreadByte()
			err = parse_header(reader, &pattern)
			if err != nil {
				return nil, err
			}
			err = parse_rle(reader, &pattern)
			if err != nil {
				return nil, err
			}
			return &pattern, nil
		case ' ', '\t', '\n':
		default:
			return nil, errors.New(fmt.Sprintf("Invalid char [%c]", c))
		}
	}
}

var RLEPatterns = map[string]string{
	"Blinker": `
#N Blinker
#O John Conway
#C A period 2 oscillator that is the smallest and most common oscillator.
#C www.conwaylife.com/wiki/index.php?title=Blinker
x = 3, y = 1, rule = B3/S23
3o!`,
	"knightshippartial": `
#N knightshippartial.rle
#C https://conwaylife.com/wiki/Partial_result
#C https://www.conwaylife.com/patterns/knightshippartial.rle
#O Josh Ball, April 2017
#C Knightship partial, later independently rediscovered in extended form by Tom Rokicki
#C and developed into a true elementary knightship, Sir Robin, by Adam P. Goucher.
#C https://conwaylife.com/wiki/Partial_result
#C https://conwaylife.com/wiki/Sir_Robin
x = 26, y = 31, rule = B3/S23
4b3o$3bo2b2o$3bo4bo2$3b3o3b2ob3o$b2obobo4bob2o$3bo2bo4bo2b2o$o2bo8bo$o
4bo2b2o2bo2bo$bob3o2b2o3b2o$3b2obob2obo3bo$9b2ob3o$9bo5bo$12b3o2b3o$
15bob2o$10b2obo3b3o$10b2o2bo2b2o$12b4o3bo$13b2obo2bo$11b2obobobo$14bo$
11bo7b2o$12b2o5b2o$14b2obo$16b3o2bobo$16b2o3bo$17bo$18b5obo$19b2o2b3o$
19b2o4bo$23b2o!`,
	"Middleweight spaceship":`
#N Middleweight spaceship
#O John Conway
#C A very well-known period 4 c/2 orthogonal spaceship.
#C www.conwaylife.com/wiki/index.php?title=Middleweight_spaceship
x = 6, y = 5, rule = B3/S23
3bo2b$bo3bo$o5b$o4bo$5o!`,
	"117P18": `
#N 117P18
#O David Buckingham
#C The first period 18 oscillator to be found. Found no later than May
#C 1991.
#C www.conwaylife.com/wiki/index.php?title=117P18
x = 39, y = 30, rule = b3/s23
37b2o$8b2o27bob$9bo25bobob$9bobo23b2o2b$10b2o13bobo2bobo6b$14bobo2bobo
2bo2bo2bo2bo5b$13bo2bo2bo2bo2bobo2bobo6b$14bobo2bobo13b2o2b$10b2o23bob
ob$9bobo25bob$9bo27b2o$8b2o29b$23b2o14b$19b2o2b2o14b$18b2o19b$19bo19b$
14b2o23b$14b2o23b$29b2o8b$2o27bo9b$bo25bobo9b$bobo23b2o10b$2b2o13bobo
2bobo14b$6bobo2bobo2bo2bo2bo2bo13b$5bo2bo2bo2bo2bobo2bobo14b$6bobo2bob
o13b2o10b$2b2o23bobo9b$bobo25bo9b$bo27b2o8b$2o!`,
	"Sir Robin": `#N Sir Robin
#O Adam P. Goucher, Tom Rokicki; 2018
#C The first elementary knightship to be found in Conway's Game of Life.
#C https://conwaylife.com/wiki/Sir_Robin
x = 31, y = 79, rule = B3/S23
4b2o$4bo2bo$4bo3bo$6b3o$2b2o6b4o$2bob2o4b4o$bo4bo6b3o$2b4o4b2o3bo$o9b
2o$bo3bo$6b3o2b2o2bo$2b2o7bo4bo$13bob2o$10b2o6bo$11b2ob3obo$10b2o3bo2b
o$10bobo2b2o$10bo2bobobo$10b3o6bo$11bobobo3bo$14b2obobo$11bo6b3o2$11bo
9bo$11bo3bo6bo$12bo5b5o$12b3o$16b2o$13b3o2bo$11bob3obo$10bo3bo2bo$11bo
4b2ob3o$13b4obo4b2o$13bob4o4b2o$19bo$20bo2b2o$20b2o$21b5o$25b2o$19b3o
6bo$20bobo3bobo$19bo3bo3bo$19bo3b2o$18bo6bob3o$19b2o3bo3b2o$20b4o2bo2b
o$22b2o3bo$21bo$21b2obo$20bo$19b5o$19bo4bo$18b3ob3o$18bob5o$18bo$20bo$
16bo4b4o$20b4ob2o$17b3o4bo$24bobo$28bo$24bo2b2o$25b3o$22b2o$21b3o5bo$
24b2o2bobo$21bo2b3obobo$22b2obo2bo$24bobo2b2o$26b2o$22b3o4bo$22b3o4bo$
23b2o3b3o$24b2ob2o$25b2o$25bo2$24b2o$26bo!`,
}

