package main

import (
	"fmt"
	"gogol"
	"time"

	"github.com/xyproto/vt100"
)

type Display struct {
	board  gogol.Board
	canvas *vt100.Canvas
}

func NewDisplay(size int, canvas *vt100.Canvas) *Display {
	display := Display{board: *gogol.NewBoard(size), canvas: canvas}
	return &display
}

func (display *Display) render() {
	var x, y int
	size := display.board.Size()
	for y = 0; y < size; y++ {
		for x = 0; x < size; x++ {
			if display.board.IsDirty(int(x), int(y)) {
				if display.board.Get(int(x), int(y)) {
					display.canvas.Plot(uint(x), uint(y), 'X')
				} else {
					display.canvas.Plot(uint(x), uint(y), ' ')
				}
			}
		}
	}
	display.canvas.Draw()
}

func main() {

	canvas := vt100.NewCanvas()
	tty, err := vt100.NewTTY()
	if err != nil {
		panic(err)
	}
	//tty.SetTimeout(50000 * time.Millisecond)
	tty.SetTimeout(50 * time.Millisecond)

	vt100.EchoOff()

	vt100.Init()
	defer vt100.Close()

	display := NewDisplay(32, canvas)
	display.board.ThrowPattern(20, 20, "Middleweight spaceship")
	display.board.ThrowPattern(15, 10, "Blinker")

	gen := 0
	for {
		display.render()
		display.board.Step()
		canvas.WriteString(0, canvas.Height()-1, vt100.White, vt100.Black, fmt.Sprintf("Generation %d", gen))
		gen++
		// time.Sleep(50 * time.Millisecond)
		switch tty.ASCII() {
		case 13, 27, 113:
			return
		}
	}

	//vt100.Clear()
	saved, _ := display.board.Save()
	fmt.Println("\n", saved)

}
