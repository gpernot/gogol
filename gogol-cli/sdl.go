package main

import (
	"fmt"
	"gogol"
	"time"
	"github.com/veandco/go-sdl2/sdl"
)

var ZOOM = 4

type Display struct {
	board    gogol.Board
	renderer *sdl.Renderer
}

func NewDisplay(size int, renderer *sdl.Renderer) *Display {
	display := Display{board: *gogol.NewBoard(size), renderer: renderer}
	return &display
}

func (display *Display) render() {
	size := int(display.board.Size())
	//display.renderer.SetDrawColor(0, 0, 0, 255)
	//display.renderer.Clear()
	rect := sdl.Rect{0, 0, int32(ZOOM), int32(ZOOM)}
	//display.renderer.SetDrawColor(255, 255, 255, 255)
	for y := 0; y < size; y++ {
		for x := 0; x < size; x++ {
			if display.board.IsDirty(x, y) {
				if display.board.Get(x, y) {
					display.renderer.SetDrawColor(255, 255, 255, 255)
				} else {
					display.renderer.SetDrawColor(0, 0, 0, 255)
				}
				display.renderer.FillRect(&rect)
			}
			rect.X += int32(ZOOM)
		}
		rect.X = 0
		rect.Y += int32(ZOOM)
	}
	display.renderer.Present()
}

func main() {

	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		panic(err)
	}
	defer sdl.Quit()

	window, err := sdl.CreateWindow("GoGOL", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		1024, 1024, sdl.WINDOW_SHOWN)
	if err != nil {
		panic(err)
	}
	defer window.Destroy()

	renderer, err := sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		panic(err)
	}
	defer renderer.Destroy()

	display := NewDisplay(int(1024/ZOOM), renderer)

	// display.board.ThrowSpaceship(2, 2)
	//display.board.ThrowPattern(2, 2, "Blinker")
	//display.board.ThrowPattern(10, 50, "Middleweight spaceship")
	display.board.ThrowPattern(140, 140, "Sir Robin")
	//display.board.ThrowPattern(40, 40, "117P18")
	paused := false
	gen := 0
	go func() {
		for {
			start := time.Now()
			start_gen := gen
			for !paused {
				display.render()
				display.board.Step()
				elapsed := time.Now().Sub(start)
				gen++
				speed := int64(gen-start_gen) * 1e9 / elapsed.Nanoseconds()
				fmt.Printf("Generation %8d\t%8d gen/s\r", gen, speed)
				//sdl.Delay(10)
			}
		}
	}()

	running := true
	for running {
		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch t := event.(type) {
			case *sdl.QuitEvent:
				println("Quit")
				running = false
				break
			case *sdl.KeyboardEvent:
				switch t.GetType() {
				case sdl.KEYDOWN:
					switch t.Keysym.Sym {
					case sdl.K_q:
						println("Quit")
						running = false
						break
					case sdl.K_SPACE:
						paused = !paused
						break
					}
				}
			}
		}
	}
}
